<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<?php
   include('link.php');
?> 

</head>
<body>
 
  <nav class="navbar navbar-expand-md navbar-dark"  style="background-color: darkcyan;">
    <a class="navbar-brand"  href="#">COVID-19</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link active"  href="#">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="#symp">Symptoms</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="#pre">Spread</a>
        </li>    
        <li class="nav-item active">
          <a class="nav-link" href="#">Prevention</a>
        </li>    
      </ul>
    </div>  
  </nav>
  <?php
   $data = file_get_contents('https://api.covid19india.org/data.json');
   $coronalive = json_decode($data,true);
  
  
   $stateCount = count($coronalive['statewise']);
   ?>
  <div class="container">
    <h1 class="headding">India Fights Corona COVID-19</h1>
     <img class="animation" src="images/corona.png" height="100px" width="150px">   
  </div>
  
  <div class="container" id="home">
   <div class="row">
    <div class="col-md-2" id="report" style="background-color: lightsalmon;">
      <h6>Confirmed</h6><p class="counter"><span><img src="images/virus-icon.png" height="40px" width="50px"></span><span><?php echo $coronalive['statewise'][0]['confirmed']?></span></p>
    </div>
    <div class="col-md-2" id="report" style="background-color: skyblue;">
    <h6>Active</h6><span><p class="counter"><span><img src="images/virus-icon.png" height="40px" width="50px"></span>
       <?php echo $coronalive['statewise'][0]['active']?></span></p></div>
    <div class="col-md-2" id="report" style="background-color: lightgreen;"><h6>Recoverd</h6><p class="counter"><span><img src="images/virus-icon.png" height="40px" width="50px"></span><?php echo $coronalive['statewise'][0]['recovered']?></p></div>
    <div class="col-md-2" id="report" style="background-color: lightslategray;"><h6>Deaths</h6><p class="counter"><span><img src="images/virus-icon.png" height="40px" width="50px"></span><?php echo $coronalive['statewise'][0]['deaths']?></p></div>
   </div>
  </div>
  <section>
   <div class="container corona-report-heading"> 
     <h1 class="headding" style="text-align:center" onclick="clickCoronaData()" >Corona Report
     <i id="icon1"  class='fas fa-plus-circle' style="font-size:24px;color:lightslategray;float:right;margin:10px"></i>
     <i id="icon2" class='fas fa-minus-circle' style="font-size:24px;color:lightslategray;float:right;display:none;margin:10px"></i>
     </h1>
     

   <div class="table-responsive text-center">
   <table class="table table-striped mt-5 text-center " id="corona-report" style="display:none; align-items:center;">
   <tr>
   <th>LastUpdate Time</th>
   <th>State</th>
   <th>StateCode</th>
   <th>Confirmed</th>
   <th>Active</th>
   <th>Deaths</th>
   <th>Recovered</th>

    
   </tr>
   
   <?php
   $i=1;
   while($i < $stateCount){
     ?>
     <tr>
     <td><?php echo $coronalive['statewise'][$i]['lastupdatedtime'] ?></td>
     <td><?php echo $coronalive['statewise'][$i]['state'] ?></td>
     <td><?php echo $coronalive['statewise'][$i]['statecode'] ?></td>
     <td><?php echo $coronalive['statewise'][$i]['confirmed'] ?></td>
     <td><?php echo $coronalive['statewise'][$i]['active'] ?></td>
     <td><?php echo $coronalive['statewise'][$i]['deaths'] ?></td>
     <td><?php echo $coronalive['statewise'][$i]['recovered'] ?></td>

     
     </tr>
     <?php
     $i++;
   }
   ?>
   </table>
  </div>
  
   </div>
   </section>

 <div class="container">
  <div class="row">
   <div class="col-md-5" id="heart"><img src="images/virus-character.jpg" height="250px" width="300px" ></div>
   <div class="col-md-7">
     </div>
     </div>
 </div>

 <!----end of the Home-->
  <div class="container" id="symp">
    <h1 class="corona-symtoms-font">Corona Symptoms</h1>
    <br>
    <div class="row">
      
      <div class="col-md-3"><img src="images/Symptoms.jpg" height="150px" width="200px"><h3 class="symtoms_font">Severe Headache</h3></div>
      <div class="col-md-3"><img src="images/Symptoms2.jpg" height="150px" width="200px"><h3 class="symtoms_font">Coughing</h3></div>
      <div class="col-md-3"><img src="images/Symptoms3.jpg" height="150px" width="200px"><h3 class="symtoms_font">Fever</h3></div>
      <div class="col-md-3"><img src="images/Symptoms4.jpg" height="150px" width="200px"><h3 class="symtoms_font">Shaking chills</h3></div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <br>
  <p class="symtoms_font_content">The COVID-19 virus affects different people in different ways.  COVID-19 is a respiratory disease and most infected people will develop mild to moderate symptoms and recover without requiring special treatment.  People who have underlying medical conditions and those over 60 years old have a higher risk of developing severe disease and death.People with mild symptoms who are otherwise healthy should self-isolate and contact their medical provider or a COVID-19 information line for advice on testing and referral.  
  People with fever, cough or difficulty breathing should call their doctor and seek medical attention. </p>
  <img src="images/stayhome.jpg" height="300px" width="100%" style="margin: 10px;">
  </div>
  </div>
</div>


  <br>
  <div class="container" id="pre">
    <h1 class="corona-symtoms-font">Prevention</h1>
    <div class="row">
      <div class="col-md-4" ><img src="images/COVID-19-2.jpg"  height="200px" width="300px"></div>
      <div class="col-md-4"><img src="images/COVID-19-3.jpg"  height="200px" width="300px"></div>
      <div class="col-md-4"><img src="images/COVID-19-4.jpg" height="200px" width="300px"></div>
  </div>
  </div>
  <br>
  <div class="container">
    <div class="row">
      <div class="col-md-4"><img src="images/COVID-19-5.jpg" height="200px" width="300px"></div>
      <div class="col-md-4"><img src="images/COVID-19-6.jpg" height="200px" width="300px"></div>
      <div class="col-md-4"><img src="images/COVID-19-7.jpg" height="200px" width="300px"></div>
  </div>
  <p class="symtoms_font_content">Based on the available evidence, including the recent publications mentioned above, WHO continues to recommend droplet and contact precautions for those people caring for COVID-19 patients. WHO continues to recommend airborne precautions for circumstances and settings in which aerosol generating procedures and support treatment are performed, according to risk assessment.13 These recommendations are consistent with other national and international guidelines, including those developed by the European Society of Intensive Care Medicine and Society of Critical Care</p>
  </div>
    <!--form -->
  <div class="form-bg-color">
  <div class="container"  style="background-color:skyblue;">
    <h1 class="corona-symtoms-font">Please fill the Assessment form</h1>
    <form>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4">First Name</label>
      <input type="text" class="form-control"  required placeholder="Enter First Name">
    </div>
    <div class="form-group col-md-6">
      <label for="inputPassword4">Last Name</label>
      <input type="text" class="form-control"  required placeholder="Enter Last Name">
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4">Mobile</label>
      <input type="number" max-length="10" class="form-control"  placeholder="Mobile">
    </div>
    <div class="form-group col-md-6">
      <label for="inputPassword4">E-mail</label>
      <input type="Email" class="form-control" placeholder="Enter Email">
    </div>
  </div>
  
  <div class="form-group">
    <label for="inputAddress2">Address 2</label>
    <input type="text" class="form-control"  placeholder="Apartment, studio, or floor">
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputCity">City</label>
      <input type="text" class="form-control" id="inputCity">
    </div>
    <div class="form-group col-md-4">
      <label for="inputState">State</label>
      <select id="inputState" class="form-control">
        <option selected>Choose...</option>
        <option>...</option>
      </select>
    </div>
    <div class="form-group col-md-2">
      <label for="inputZip">Zip</label>
      <input type="text" class="form-control" id="inputZip">
    </div>
  </div>
  <div class="form-group">
    <div class="form-check">
    <span style="margin:10px;"><input class="form-check-input" type="checkbox" id="parent" onclick="selectAll()"  />
      <label class="form-check-label" for="couch"><p>Select All</p></label ></span><br>
      
     <span style="margin:10px;"><input class="form-check-input chek" type="checkbox" id="child" onclick="selectAll()"  />
      <label class="form-check-label" for="child"><p>Couch</p></label></span>
      
      <span style="margin:10px;"><input class="form-check-input" type="checkbox" id="child1" onclick="selectAll()" />
      <label class="form-check-label" for="fever">Fever</label></span>

      <span style="margin:10px;"><input class="form-check-input" type="checkbox" id="child2" onclick="selectAll()" />
      <label class="form-check-label" for="breathing">Difficulty Breathing</label></span>
      <span style="margin:10px;"><input value="{!v.first}" class="form-check-input" id="none" type="checkbox" id="child3" onclick="selectAll()" />
      <label class="form-check-label" for="gridCheck">none</label></span>

    </div>
  </div>
  <button type="submit" class="btn btn-primary">Sign in</button>
</form>
    

  </div>
</div>

  

<!-- //////////////////////// Top cursor ////////////////////-->
 <div class="container scrolltop floa-right pr-5 ">
  
   <b class="fa fa-arrow-up mybtn" onclick="topFunction()"  id="mybtn">click..</b>
 </div>


 <script>
 function selectAll(){
     parent = document.getElementById("parent");
     var first = document.getElementById("child");
     var second = document.getElementById("child1");
     var third =document.getElementById("child2");
     var none = document.getElementById("none");

 if(parent.checked === true){
   first.checked = true;
   second.checked = true;
   third.checked = true;
 }
  if(none.checked === true){
    parent.checked =false;
    first.checked = false;
    second.checked = false;
    third.checked = false;
 }
}
const queryString = window.location.search;
console.log(queryString);

  
var mybutton = document.getElementById("myBtn");

// When the user scrolls down 20px from document, show the button
 window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
   // mybutton.style.display = "block";
  } else {
   // mybutton.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}







// here we are code for click to display data

function clickCoronaData(){
  var corona =  document.getElementById('corona-report');
  if (corona.style.display == "none" &&  icon2.style.display == "none" ) {
    corona.style.display = "block";
    icon2.style.display = "block";
   icon1.style.display = "none";
  } else {
    corona.style.display = "none";
    icon2.style.display = "none";
    icon1.style.display = "block";
   
  }
}


// for counter of live data

$('.counter').counterUp({
    delay: 10,
    time: 1000
});

// for icon of corona report
function iconButton(){
var icon1 = document.getElementById('icon1');
var icon2 = document.getElementById('icon2');
if(icon2.style.display == "none"){
   icon2.style.display = "block";
   icon1.style.display = "none";
}

else{
  icon2.style.display = "none";
  icon1.style.display = "block";
}
}





</script>  


</body>
</html>
